**WARNING, ORBTK (used by this project) WAS DISCONTINUED AND NO LONGER COMPILES OR WORKS**

**build instructions**
1. install rust from https://rustup.rs/

2. this program is made using rust, so if you want to build it from source, download or clone the repository, navigate to the cloned or downloaded directory,

3. compile and run the program with "cargo run --release"


**Features**
* uses regex pattern matching to find 5 letter words to help with wordle solving!
* outputs list of words that match the regex. 


**Instructions for use**
1. use an astrix to denote 1 or more of any character
example:

a\*b\*  displays a list of five letter words that start with a and then some number of chars later has a b, then at least one character following it

